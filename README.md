## 博客链接
点我学习：[前后端交互的方式整理](https://www.cnblogs.com/Candies/p/10155525.html)
#### 数据交互格式
可以通过GET/POST等方式将变量、对象、数组、JSON、XML、file/img、base64图片、cookies等各种形式的数据提交到后端。
###### GET
代码示例：
```
/**
 * GET-变量
 */
@GetMapping("/testVar1")
public String testVar1(/*@RequestParam("param")*/String param) {
    return param;
}

/**
 * GET-path变量
 */
@GetMapping("/testVar2/{param}")
public String testVar2(@PathVariable/*("param")*/ String param) {
    return param;
}

/**
 * GET-对象
 */
@GetMapping("/testObject1")
public GetParam testObject1(GetParam obj) {
    return obj;
}

/**
 * GET-数组
 */
@GetMapping("/testArray1")
public Object testArray1(String[] obj) {
    return obj;
}
```
运行结果：
![](http://weizhiprod.oss-cn-hangzhou.aliyuncs.com/01.getVar1.jpg)
![](http://weizhiprod.oss-cn-hangzhou.aliyuncs.com/01.getVar2.jpg)
![](http://weizhiprod.oss-cn-hangzhou.aliyuncs.com/01.getObject1.jpg)
![](http://weizhiprod.oss-cn-hangzhou.aliyuncs.com/01.getArray1.jpg)
###### POST
POST方式可以使用curl、postman、getman、eolinker等工具实现。
```
/**
 * POST-多个变量、对象（form提交）
 */
@PostMapping(value = "/testPostFormVar")
public String testPostVar1(@RequestParam(name = "username") String username,
                          @RequestParam(name = "password") String password) {
    return username + ":" + password;
}

/**
 * POST-JSON（ajax）
 */
@PostMapping(value = "/testPostJsonVar")
public PostParam testPostVar2(@RequestBody PostParam param) {
    return param;
}

/**
 * 通用的post接收方式
 */
@PostMapping(value = "/testPostVar")
public PostParam testPostVar(PostParam param) {
    return param;
}
```
运行结果：
![](http://weizhiprod.oss-cn-hangzhou.aliyuncs.com/02.post1.jpg)
![](http://weizhiprod.oss-cn-hangzhou.aliyuncs.com/02.post2.jpg)
![](http://weizhiprod.oss-cn-hangzhou.aliyuncs.com/02.post3.jpg)
![](http://weizhiprod.oss-cn-hangzhou.aliyuncs.com/02.post4.jpg)
![](http://weizhiprod.oss-cn-hangzhou.aliyuncs.com/02.post5.jpg)

POST方式总结：

| content-type | 数据格式|后台接受方式|专用注解|
| ------ | ------ | ------ |------ |
| application/x-www-form-urlencoded[默认] | json对象 |对象|@RequestParam|
| application/json | json字符串 |对象|@RequestBody|
| $.post() 默认application/x-www-form-urlencoded| json对象 |对象|@RequestParam|