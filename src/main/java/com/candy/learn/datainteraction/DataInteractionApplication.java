package com.candy.learn.datainteraction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataInteractionApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataInteractionApplication.class, args);
    }

}

