package com.candy.learn.datainteraction.controller;

import com.candy.learn.datainteraction.dto.GetParam;
import com.candy.learn.datainteraction.dto.PostParam;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>Title: DataInteractionController</p>
 *
 * <p>Description: 前后端数据交互</p>
 */
@RestController
@RequestMapping("/dataInteraction")
public class DataInteractionController {

    /**
     * GET-变量
     */
    @GetMapping("/testVar1")
    public String testVar1(/*@RequestParam("param")*/String param) {
        return param;
    }

    /**
     * GET-path变量
     */
    @GetMapping("/testVar2/{param}")
    public String testVar2(@PathVariable/*("param")*/ String param) {
        return param;
    }

    /**
     * GET-对象
     */
    @GetMapping("/testObject1")
    public GetParam testObject1(GetParam obj) {
        return obj;
    }

    /**
     * GET-数组
     */
    @GetMapping("/testArray1")
    public Object testArray1(String[] obj) {
        return obj;
    }

    /**
     * POST-多个变量、对象（form提交）
     */
    @PostMapping(value = "/testPostFormVar")
    public String testPostVar1(@RequestParam(name = "username") String username,
                              @RequestParam(name = "password") String password) {
        return username + ":" + password;
    }

    /**
     * POST-JSON（ajax）
     */
    @PostMapping(value = "/testPostJsonVar")
    public PostParam testPostVar2(@RequestBody PostParam param) {
        return param;
    }

    /**
     * 通用的post接收方式
     */
    @PostMapping(value = "/testPostVar")
    public PostParam testPostVar(PostParam param) {
        return param;
    }

    /**
     * POST-File
     */
    @PostMapping(value = "/testFile")
    public String testFile(@RequestParam("file") MultipartFile uploadFile) {
        return uploadFile.getOriginalFilename();
    }
}
