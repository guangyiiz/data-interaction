package com.candy.learn.datainteraction.controller;

import com.candy.learn.datainteraction.dto.PostParam;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>Title: CrossDomainRequestsController</p>
 *
 * <p>Description: 跨域</p>
 *
 * @date 2018/12/20
 */
@RestController
@RequestMapping("/cdr")
// @CrossOrigin // 跨域方案一
public class CrossDomainRequestsController {
    /**
     * testCdr
     */
    @RequestMapping(value = "/testCdr")
    public PostParam testCdr(PostParam param) {
        return param;
    }

    /**
     * testJsonp
     */
    @RequestMapping(value = "/testJsonp")
    public JSONPObject testJsonp(PostParam param, String callback) {
        return new JSONPObject(callback, param);
    }
}
