package com.candy.learn.datainteraction.dto;

/**
 * <p>Title: GetParam</p>
 *
 * <p>Description: </p>
 *
 * @date 2018/12/17
 */

public class GetParam {
    String text;
    String value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
