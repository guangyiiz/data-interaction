package com.candy.learn.datainteraction.dto;

/**
 * <p>Title: PostParam</p>
 *
 * <p>Description: </p>
 *
 * @date 2018/12/18
 */

public class PostParam {

    String username;
    String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
